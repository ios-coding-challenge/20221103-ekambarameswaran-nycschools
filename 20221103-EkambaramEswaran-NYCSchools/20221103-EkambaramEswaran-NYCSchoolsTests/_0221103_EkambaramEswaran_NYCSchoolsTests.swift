//
//  _0221103_EkambaramEswaran_NYCSchoolsTests.swift
//  20221103-EkambaramEswaran-NYCSchoolsTests
//
//  Created by Ekambaram E on 11/3/22.
//

import XCTest
@testable import _0221103_EkambaramEswaran_NYCSchools

final class _0221103_EkambaramEswaran_NYCSchoolsTests: XCTestCase {

    var viewModel: NYCSchoolHomeViewModel?
    var detailViewModel: NYCSchoolDetailViewModel?
    var networkManager: MockNetworkManager?
    
    func testNeworkService() throws {
        
        //Given
        let schoolData = [NYCSchoolHomeModel(dbn: "28Q620", schoolName: "he use of remote response ", primaryAddressLine1: "students to new experien", city: "tudents remain wit", zip: "60656", stateCode: "change, students remain with their House and advisor until graduation. Our students graduate with the", overveiwParagraph: "change, students remain with ")]
        networkManager = MockNetworkManager()
        networkManager?.schoolResponse = schoolData
        viewModel = NYCSchoolHomeViewModel(apiService: networkManager ?? MockNetworkManager())
       
        //When
        viewModel?.callFuncToGetSchoolsData()
        
        //Then
        let zipCode = try XCTUnwrap(viewModel?.schoolsData?[0].zip)
        XCTAssertEqual(zipCode, "60656")
    }
    
    func testDetailScreenService() throws {
        
        //Given
        let schoolDetail = [NYCSchoolDetailModel(dbn: "28Q620", schoolName: "Archimedes Academy for Math", numOfSatTestTakers: "and the use of remote response systems. Our non-traditional physical education classes expose students to new experiences that", satCriticalReadingAvgScore: "change, students remain with their House and advisor until graduation. Our students graduate with the knowledge of math", satMathAvgScore: "use of remote response systems", satWritingAvgScore: "88")]
        networkManager = MockNetworkManager()
        networkManager?.schoolDetail = schoolDetail
        detailViewModel = NYCSchoolDetailViewModel(schoolId: "28Q620", apiService: networkManager ?? NetworkManager())
       
        //When
        detailViewModel?.callFuncToGetSchoolDetailData()
        
        //Then
        let satWritingAvgScore = try XCTUnwrap(detailViewModel?.schoolDetails?[0].satWritingAvgScore)
        XCTAssertEqual(satWritingAvgScore, "88")
    }
    
    func testNetworkManagerCall() throws {
        let expectation = expectation(description: "Fetch API")
        let networkManager = NetworkManager()
        if let url = URL(string: NYCSchoolAPIEndpoints.schoolsAPI) {
            networkManager.fetchAPIService(url, model: [NYCSchoolHomeModel].self) { result in
                XCTAssertNotNil(result)
                expectation.fulfill()
            }
        }
        waitForExpectations(timeout: 2.0)
    }

}


class MockNetworkManager: NetworkManagerProtocol {
   
    var schoolResponse: [NYCSchoolHomeModel]?
    var schoolDetail: [NYCSchoolDetailModel]?
    
    func fetchAPIService<T>(_ url: URL, model: T.Type, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable, T : Encodable {
        if let school = schoolResponse as? T {
            completion(.success(school))
        } else if let detail = schoolDetail as? T {
            completion(.success(detail))
        }
    }
}
