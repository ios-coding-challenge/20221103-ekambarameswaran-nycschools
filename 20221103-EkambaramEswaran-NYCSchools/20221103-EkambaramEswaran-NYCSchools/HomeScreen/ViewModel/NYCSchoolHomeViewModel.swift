//
//  NYCSchoolHomeViewModel.swift
//  20221103-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 11/3/22.
//

import Foundation

//MARK: ViewModel class for Home screen
class NYCSchoolHomeViewModel: NSObject {
    
    private var apiService : NetworkManagerProtocol?
   
    private(set) var schoolsData : [NYCSchoolHomeModel]? {
        didSet {
            self.bindNYCSchoolHomeViewModelToController()
        }
    }
    
    var bindNYCSchoolHomeViewModelToController : (() -> ()) = {}
    
    init(apiService: NetworkManagerProtocol = NetworkManager()) {
        super.init()
        self.apiService = apiService
    }
    
    //MARK: Making service call for Home screen | getting list of school
    func callFuncToGetSchoolsData() {
        
        if let url = URL(string: NYCSchoolAPIEndpoints.schoolsAPI) {
            self.apiService?.fetchAPIService(url, model: [NYCSchoolHomeModel].self, completion: { [weak self] result in
                switch result {
                case .success(let response):
                    self?.schoolsData = response
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
    }
}

