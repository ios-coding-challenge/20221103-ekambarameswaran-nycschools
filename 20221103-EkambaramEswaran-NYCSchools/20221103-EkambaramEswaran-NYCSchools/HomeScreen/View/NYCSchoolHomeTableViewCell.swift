//
//  NYCSchoolHomeTableViewCell.swift
//  20221103-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 11/3/22.
//

import UIKit

//MARK: Home screen list of school cell
class NYCSchoolHomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel?
    @IBOutlet weak var schoolAddress: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(school: NYCSchoolHomeModel?) {
       
        if let schoolCode = school?.dbn, let schoolName = school?.schoolName {
            self.schoolName?.text = schoolCode + " | " + schoolName
        } else {
            //
        }
        
        if let primaryAdress = school?.primaryAddressLine1,
           let stateCode = school?.stateCode,
           let zip = school?.zip {
            self.schoolAddress?.text =  "\(primaryAdress), \(stateCode), \(zip)"
        } else {
            //
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
