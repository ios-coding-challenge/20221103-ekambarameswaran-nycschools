//
//  NYCSchoolHomeViewController.swift
//  20221103-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 11/3/22.
//

import UIKit

class NYCSchoolHomeViewController: NYCSchoolBaseViewController {
    
    
    @IBOutlet weak var schoolsTableView: UITableView?
    var nycSchoolHomeViewModel: NYCSchoolHomeViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callToViewModelForUIUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func callToViewModelForUIUpdate(){
        showActivityIndicator()
        self.nycSchoolHomeViewModel = NYCSchoolHomeViewModel()
        self.nycSchoolHomeViewModel?.callFuncToGetSchoolsData()
        self.nycSchoolHomeViewModel?.bindNYCSchoolHomeViewModelToController = {
            DispatchQueue.main.async {
                self.hideActivityIndicator()
                self.schoolsTableView?.reloadData()
            }
        }
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource
extension NYCSchoolHomeViewController: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nycSchoolHomeViewModel?.schoolsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NYCSchoolHomeTableViewCell", for: indexPath) as? NYCSchoolHomeTableViewCell else {
            return UITableViewCell()
        }
        cell.configureCell(school: self.nycSchoolHomeViewModel?.schoolsData?[indexPath.row])
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.systemGray6 : .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       navigateToDetailView(selectedSchool: self.nycSchoolHomeViewModel?.schoolsData?[indexPath.row])
    }
    
    func navigateToDetailView(selectedSchool: NYCSchoolHomeModel?) {
       
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        guard let detailViewController = storyBoard.instantiateViewController(
            withIdentifier: "NYCSchoolDetailViewController") as? NYCSchoolDetailViewController
        else { return }
        detailViewController.selectedSchool = selectedSchool
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

