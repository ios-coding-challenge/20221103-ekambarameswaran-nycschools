//
//  NYCSchoolDetailViewController.swift
//  20221103-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 11/3/22.
//

import UIKit


class NYCSchoolBaseViewController: UIViewController {
    
    var activityView : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView ?? UIActivityIndicatorView())
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityView?.stopAnimating()
    }
    
}


class NYCSchoolDetailViewController: NYCSchoolBaseViewController {

    var selectedSchool: NYCSchoolHomeModel?
    var nycSchoolDetailViewModel: NYCSchoolDetailViewModel?
    
    @IBOutlet weak var numSatTakersLabel: UILabel?
    @IBOutlet weak var critialReadinfAvgLabel: UILabel?
    @IBOutlet weak var mathAvgLabel: UILabel?
    @IBOutlet weak var writingAvgScoreLabel: UILabel?
    @IBOutlet weak var schoolDetailLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedSchool?.schoolName
        callToViewModelForUIUpdate(schoolDetail: self.nycSchoolDetailViewModel?.schoolDetails)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func callToViewModelForUIUpdate(schoolDetail: [NYCSchoolDetailModel]?){
        showActivityIndicator()
        self.nycSchoolDetailViewModel = NYCSchoolDetailViewModel(schoolId: selectedSchool?.dbn ?? "")
        self.nycSchoolDetailViewModel?.callFuncToGetSchoolDetailData()
        self.nycSchoolDetailViewModel?.bindNYCSchoolDetailViewModelToController = {
            DispatchQueue.main.async {
                if let updatedSchoolDetail = self.nycSchoolDetailViewModel?.schoolDetails?.first {
                    self.numSatTakersLabel?.text = updatedSchoolDetail.numOfSatTestTakers
                    self.critialReadinfAvgLabel?.text = updatedSchoolDetail.satCriticalReadingAvgScore
                    self.mathAvgLabel?.text = updatedSchoolDetail.satMathAvgScore
                    self.writingAvgScoreLabel?.text = updatedSchoolDetail.satWritingAvgScore
                }
                self.schoolDetailLabel?.text = self.selectedSchool?.overveiwParagraph
                self.hideActivityIndicator()
            }
        }
    }
}
