//
//  NYCSchoolConstant.swift
//  20221103-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 11/3/22.
//

import Foundation

//MARK: List of API service calls
struct NYCSchoolAPIEndpoints {
    static let schoolsAPI = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schoolDetailsAPI = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}

